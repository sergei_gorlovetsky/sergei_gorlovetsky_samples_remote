import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'fetchdata',
    template: require('./fetchdata.component.html')
})
export class FetchDataComponent {
    public visitStats: visitStat[];
    public dataNotFound: boolean = false;
    link = 'http://api.giphy.com/v1/gifs/search?api_key=dc6zaTOxFJmzC&q=';
    http: Http;

    
    constructor(public fb: FormBuilder, http: Http) {
        this.http = http;
    }

    public topFiveSitesForm = this.fb.group({
        searchDate: [Date.now(), Validators.required]
    });
    doSearch(event) {
        console.log(event);
        console.log(this.topFiveSitesForm.controls.searchDate.value);
        this.GetTopFive(this.topFiveSitesForm.controls.searchDate.value);


    }

    public GetTopFive(date: string) {
    
        this.http.get('/api/SampleData/GetTopFive/?date=' + date).subscribe(result => {
            this.visitStats = result.json();
            if (this.visitStats.length == 0) {
                this.dataNotFound = true;
            }
            else {
                this.dataNotFound = false;
            
            }
        });

    }
}

interface visitStat {
    id: number;
    websiteURL: string;
    visitDate: Date;
    visitCount: number;
    
}
