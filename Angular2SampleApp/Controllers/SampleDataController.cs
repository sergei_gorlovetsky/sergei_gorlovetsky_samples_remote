using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Angular2SampleService.DataModel;
using Angular2SampleService;
using Angular2SampleService.DataRepository;

namespace Angular2SampleApp.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
       
        private IVisitStatsRepository _visitStatsRepository;

        public SampleDataController(IVisitStatsRepository visitsRepository)
        {
            _visitStatsRepository = visitsRepository;

        }

        [HttpGet("[action]")]
        public IEnumerable<VisitStats> GetTopFive(DateTime date)
        {
            DataService ds = new DataService(_visitStatsRepository);
            return ds.GetTop(date, 5);
        }

       
    }
}
