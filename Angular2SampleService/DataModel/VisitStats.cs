﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angular2SampleService.DataModel
{
    public class VisitStats: IEntityBase
    {
        public int Id { get; set; }
        public string WebsiteURL { get; set; }
        public DateTime VisitDate { get; set; }
        public long VisitCount { get; set; }
    }
}
