﻿using Angular2SampleService.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angular2SampleService.DataRepository
{
    public interface IVisitStatsRepository : IEntityBaseRepository<VisitStats> { }
}
