﻿using Angular2SampleService.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angular2SampleService.DataRepository
{
     public class VisitStatsRepository : EntityBaseRepository<VisitStats>, IVisitStatsRepository
    {
        public VisitStatsRepository(VisitStatsContext context)
            : base(context)
        { }
    }
}
