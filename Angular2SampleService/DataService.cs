﻿using Angular2SampleService.DataRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angular2SampleService
{
    public class DataService
    {
        private IVisitStatsRepository visitStatsRepository;
        public DataService(IVisitStatsRepository rep)  {
            visitStatsRepository = rep;
        }
           

        public IEnumerable<DataModel.VisitStats> GetTop(DateTime date, int top)
        {
            IEnumerable<DataModel.VisitStats> topList = visitStatsRepository
               .GetAll()
               .Where(d => d.VisitDate == date)
               .OrderByDescending(s => s.VisitCount)
               .Take(top)
               .ToList();

            return topList;
        }

    }

}

