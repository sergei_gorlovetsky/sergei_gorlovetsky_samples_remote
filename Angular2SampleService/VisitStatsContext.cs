﻿using Angular2SampleService.DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angular2SampleService
{
       public class VisitStatsContext : DbContext
    {
        public DbSet<VisitStats> VisitStats { get; set; }

        public VisitStatsContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
              modelBuilder.Entity<VisitStats>()
                .ToTable("VisitStats");

       

        }
    }
}
